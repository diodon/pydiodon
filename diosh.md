# Diosh

## What it is  

`diosh` is a DSL (Domain Specific Language) which enables to pilot `pydiodon.py` with human friendly commands in a Linux console.    

## How it works


`pydiodon` is a library with function processing some arguments. One way to use them is to call them with the required arguments. The list of arguments may be large, and it is easy to forget which one to call, its format, and its significance. `diosh` is one way to overcome this difficulty. The way it works is as follows:   

* instead, for example, of calling `Y, L, V = pca(A, pretreatment="standard", k=-1, meth="svd")`, the user simply calls `do pca`;
* the arguments and their values are stored as key: value in a yaml file, which can be filled on the fly with some help;  
* the outputs (here Y, L, V) are stored as attribute of a unique hidden class for possible further processing; 
* there is a hidden layer between diosh and pydiodon building the link between the arguments in the yaml file and the functions in pydiodon;
* `diosh` provides some help on the fly (names of the arguments, possible values, link with the doc strings and sphinx) to fill the yaml file.


### Recommandation ###

`diosh` is convenient if one wants to explore some choices by changing some arguments, or to run standard procedures with existing yaml file. 

## Diosh gallery

To run a gallery of what `diosh` can do:   
* open a new console, where you want
* type  
```sh
$ diosh gallery gallery  
```
* follow what appears in the console:
    * in green, the diosh commands,
    * in white, a short description of what it does.

For each command, a pop-up window appears, with proposed values for the arguments of the functions. For a discovery, you can accept them by clicking on `run`. For learning, you can choose them (you can modify them, save the modification, and run the command with the new arguments). The significance of the arguments is explained in the sphinx documentation, accessible at  https://diodon.gitlabpages.inria.fr/pydiodon. Each diosh command has the name of a pydiodon function, expecting the same arguments provided by the yaml file.  


## Getting started

* select a directory and go into it  
* select a dataset, here `diatoms_sweden`, and copy it in the directory, with `pydiodon`, with   

```python
# import pydiodon
>>> import pydiodon as dio  

# copy the dataset in the directory where you are 
>>> dataset	= "diatoms_sweden"   
>>> dio.copy_dataset(dataset)   
```  

Then, the file has been copied:    

```sh
$ ls 
diatoms_sweden.txt  
```   

Note that diatoms_sweden is a label for the data set, the name of which is diatoms_sweden.txt. This is handled by `pydiodon` for data sets in the tutorial.   

A session is started by typing   

```sh
$ diosh diatoms_sweden
``` 

The DSL is looking for a `yaml` file `diatoms_sweden.yaml`, compulsory for `diosh`. As it does not exist, it is proposed to cretae one:  

```sh
$ diatoms_sweden.yaml does not exist. Should I create it ? (y/n) 
``` 

Type `ỳes`. An empty yaml file will be created, and `diosh` is ready for running.  During a session, the prompt of the console will display the name of the project to be run, like 
```sh
dio:diatoms_sweden-> 
``` 


## A short session  

Here we show how to run a short session: performing the PCA of the data set, and plotting the point cloud of principal components.  

All commands are 

```sh  
dio:diatoms_sweden-> do <method>  
``` 

where the `method` is a function defined in library `pidiodon.py`. There is one section in the yaml file with the values to be transferred to the function as arguments. Either the yaml file is ready, or the values are to be given. It can be done by filling fields in a pop-up window.   

* First step is to download the dataset. This is done by calling method `load pca_file`, as in   

```sh
dio:diatoms_sweden-> do load pca_file 
```

We suggest to select the data set file by clicking on `change`, setting row names and colnames to `yes`, and delimiters as `tab`. These values when selected can be saved in the yaml file fo further use. Continue by clicking on `run`.   

* Then, PCA can be run by calling    
```sh  
dio:diatoms_sweden-> do pca  
```  
* Point cloud can be plotted by calling 
```sh  
dio:diatoms_sweden-> do plot_component_scatter 
```

* Parallel coordinates can be plotted by calling
```sh
dio:diatoms_sweden-> do plot_components_splines
```

* and the correlation circle of old variables with new axis by calling 
```sh
dio:diatoms_sweden-> do plot_var
```

* It is possible to run all commands with one line by typing 
```sh
dio:diatoms_sweden-> do load pca_file pca plot_components_scatter plot_var
```

* It is possible as well to write with a notepad an ascii file named for example diatoms_sweden.yap (*yet another program ...*), like  
```sh 
# standard procedure for PCA
load pca_file      
pca   
plot_components_scatter   
plot_var  
```  

Then, this standard procedure can be run by 
```sh 
dio:diatoms_sweden-> run diatoms_sweden.yap
```

* To leave a session, just type 
```sh 
dio:diatoms_sweden-> quit()
``` 

## A survival kit ...

Here are some useful commands when in diosh:  

### <kbd>*</kbd> General purpose

| Command | what it does |
|----------|-----------------------|
| <kbd>Tab</kbd> | provides list of diosh commands |  
| ! <kbd>shell command</kbd> | runs the shell comand as if in a terminal | 
| check &lt;method&gt; | displays a pop up windows for choosing/modifyng arguments in yaml file | 
| do | lists all available methods | 
| do  &lt;method&gt; | runs method <method> with arguments in the yaml file |
| run &lt;program.yap&gt; | runs all the lines in program.yap sequentially |

### For running methods 

| Command | what it does |
|----------|-----------------------|
| <kbd>Tab</kbd> | provides list of diosh commands |  
| check &lt;method&gt; | displays a pop up windows for choosing/modifyng arguments in configuration file | 
| do | lists all available methods | 
| do <kbd> </kbd> | list of available methods with autocompletion | 
| do  &lt;method&gt; | runs method <method> with arguments in the yaml file |


### For documentation

| Command | what it does |
|----------|-----------------------|
| doc | launches a browser on `pydiodon` documentation pages |
| doc &lt;method&gt; | displays the documentation of the method as processed by sphinx |
| help &lt;method&gt; | displays the docstring of the method in the console |







