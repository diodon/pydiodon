# Jupyter notebooks

## Warning

For those who wish to play with the interactive version *.ipynb of a notebook, it is advised to :
- create a directory not followed by the git and copy the notebooks, as
```sh
# be in $pydiodon
$mkdir my_notebooks

# copy the notebooks
$cd my_notebooks
$cp ../jupyter/*.ipynb .
```
- change the names, parameters, values in this directory only. Indeed, diodon team may wish to update a jupyter notebook, and push the change on the git. If the user has changeed himself/herself the same notebook, there will be a cnflict of versions.

- for the notebooks used as tutorials with provided datasets, do not forget to update the relatoive âth to the directory `datasets`.

## Tutorials 

Several notebooks are available as tutorials, one for one method, as follows

| Notebook | method | ipynb | html |
|----------|--------|-------|------|
| pca_tutorial | PCA | yes | yes|
| coa_with_diodon | CoA | yes | yes|
| mds_with_diodon | MDS | yes | yes|


The notebooks are given in two formats:
- ipynb, where they are interactive
- html, where they are frozen

The ipynb format permits to interact with the code (but see the warning section above).  
The html format permits to check quickly a standard analysis.  


## 




