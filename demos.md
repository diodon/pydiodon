# Demos

Some short programms are proposed in directory `demos` as demonstrations of main tools: PCA, MDS and CoA.  



### For a demo of PCA

* go into directory `demos`   

* in the console, type   
```sh
$ ./pca_demo.py &      
```


### For a demo of CoA

* go into directory `demos`   

* in the console, type   
```sh
$ ./coa_demo.py  &  
```


### For a demo of MDS

* go into directory `demos`   

* in the console, type   
```sh
$ ./mds_demo.py  &    
```


## Notes   

* These programs are those detailed in Jupyter notebooks, with same names. 

* Plotting is done with the choice that windows once displayed do not block the course of the program. Hence, all windows produced by one program coexist on the screen, and can be manipulated.  The `&` means that once it is terminated, the program hands over to the user, who can terminate it by typing on the key <kbd>enter</kbd>. All windows have to be closed by hand. Typing <kbd>Ctrl</kbd>+<kbd>C</kbd> may lead to unexpected events. 

* If the user wants to modify these programs, he/she is advised either to rename them or to copy them in another directory. Indeed, <kbd>Diodon team</kbd> may wish to modify them in the gitlab repository. In such a case, there may be some conflicts if the user has modified them as well and wants to download the last version with a <kbd>Git Pull</kbd>.

