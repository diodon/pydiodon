#!/usr/bin/env python3
#

import pydiodon as dio


data_file	= "../datasets/diatoms_sweden.txt"
A, rn, cn	= dio.load_ascii(filename=data_file, delimiter="\t", colnames=True, rownames=True, dtype=float)
Y, L, V		= dio.pca(A, pretreatment="standard", k=-1, meth="svd")
#
dio.plot_components_scatter(Y)
dio.plot_var(V, varnames=cn, axis_1 = 1, axis_2 = 2, title=None, x11=True, plotfile=None, fmt="png")
dio.plot_eig(L, k=-1, frac=True, cum=True, Log=False, dot_size=20, col="red", title=None, pr=True, x11=True, plotfile=None, fmt="png")

Qual_axis, Qual_cum = dio.quality(Y)

dio.plot_quality(Qual_axis, Qual_cum, r=2, cum=False, sort=False, col="blue", title=None, x11=True, plotfile=None, fmt="png")
