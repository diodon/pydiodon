#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pydiodon as dio

disfile	= "../datasets/guiana_trees.sw.dis"

A, rn, cn	= dio.load_ascii(disfile)
Y, L		= dio.mds(A)
dio.plot_eig(L, cum=True)
dio.plot_components_scatter(Y)
dio.plot_components_splines(Y, n_axis=100)

