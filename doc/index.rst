.. pydiodon documentation master file, created by
   sphinx-quickstart on Thu Feb 18 10:44:39 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pydiodon's documentation!
====================================

.. toctree::
   :maxdepth: 4

.. automodule:: pydiodon

Functions
=========

.. autosummary::
   :toctree: fonctions
   :nosignatures:
   
    coa
    mds
    pca
    mat_evd
    svd_grp
    bicentering
    centering_operator
    center_col
    center_row
    scale
    get_correlation_matrix
    dis2gram
    pca_core
    pca_met
    pca_iv
    project_on
    loadfile
    load_ascii
    load_hdf5
    load_dataset
    writefile
    write_ascii
    write_hdf5
    quality
    plot_eig
    plot_components_scatter
    plot_components_splines
    plot_components_quality
    plot_var
    plot_var_heatmap
    plot_correlation_matrix
    plot_coa
    plot_quality
    hovering
    plot_components_heatmap

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

